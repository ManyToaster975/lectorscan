# LectorScan

Proyecto realizado en el IDE Android Studio en su version 3.4 y Gradle 3.4 

---
## Detalles del proyecto
- Lenguaje: JAVA
- SQLITE->Login
- Kit AA Machine Learning->Firebase 16.0.0 y 16.0.1
- Heroku->Servidor (En realidad puede ser cualquier servidor para extraer la informacion)
- Version: 1.0

---
## Descripcion
Es una aplicacion en la cual te puedes conectar por medio de un usuario y una constraseña, en caso de no contar puedes registrarte
para que tu cuenta se quede almacenada en el dispositivo. Al estar en el inicio puedes tomar una foto a algun numero o letra que
se almacenara en tu Galeria para que el programa intente reconocer el patron. 
En caso de detectar algo se reflejara lo que detecto en la imagen y en caso de que lo desee el programador realizara un request 
a un servidor externo o hara lo que el programador desee. 

Nota: La foto se debe tomar de forma horizontal para una mejor precision.

---
## Instalacion
Puede compilarlo por medio de algun IDE adaptable por una AVM, o desde su dispositivo, o crear e instalar el APK. Solo se permite
en Android 4.3 posterior.

---
## Imagenes
![Imgur](https://i.imgur.com/psT1eMp.png)
---
![Imgur](https://i.imgur.com/o1wBdnF.png)
---
![Imgur](https://i.imgur.com/AAKPgAD.png)
![Imgur](https://i.imgur.com/UBrnwpQ.png)
### Numero Incorrecto
---
![Imgur](https://i.imgur.com/qlcPuvr.png)
![Imgur](https://i.imgur.com/YxGeaoT.png)
---
### Numero Correcto

