package com.apps.proyectos.scanpicture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector;

import java.util.List;

public class MenuInicio extends AppCompatActivity {

    private ImageView VistaImagen;
    private TextView TextoImagen;

    private Bitmap imageBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_inicio);

        VistaImagen=findViewById(R.id.vistaImagen);
        TextoImagen=findViewById(R.id.textoImagen);

        Button btnDetectar = findViewById(R.id.btnDetectar);
        btnDetectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detectarTexto();
            }
        });

        Button btnTomar = findViewById(R.id.btnTomar);
        btnTomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        Button btnRotar = findViewById(R.id.btnRotar);
        btnRotar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VistaImagen.setRotation(VistaImagen.getRotation()+90);
            }
        });

        Button btnIr = findViewById(R.id.btnIr);
        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextoImagen.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "No hay contenido traducido!", Toast.LENGTH_SHORT).show();
                }else{
                    finish();
                    Intent ver=new Intent(MenuInicio.this, EstadoFoto.class);
                    ver.putExtra("dato", TextoImagen.getText().toString());
                    startActivity(ver);
                }
            }
        });
    }

    private void detectarTexto() {
        try{
            FirebaseVisionImage imagen=FirebaseVisionImage.fromBitmap(imageBitmap);
            FirebaseVisionTextDetector detector= FirebaseVision.getInstance().getVisionTextDetector();
            detector.detectInImage(imagen).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                @Override
                public void onSuccess(FirebaseVisionText firebaseVisionText) {
                    procesarTexto(firebaseVisionText);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void procesarTexto(FirebaseVisionText text){
        List<FirebaseVisionText.Block> bloques=text.getBlocks();
        if(bloques.size()==0){
            Toast.makeText(this, "No hay texto, favor de volver a tomar la imagen", Toast.LENGTH_SHORT).show();
            return;
        }
        for(FirebaseVisionText.Block bloque:text.getBlocks()){
            String texto=bloque.getText();
            TextoImagen.setText(texto);
        }
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    //Get the Thumbnail
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            assert extras != null;
            imageBitmap = (Bitmap) extras.get("data");
            VistaImagen.setImageBitmap(imageBitmap);

        }
    }


}
