package com.apps.proyectos.scanpicture;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registro extends AppCompatActivity {

    Button BtnVaciar, BtnRegistrar;
    EditText CajaUsuario, CajaCorreo, CajaMatricula, CajaContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        CajaUsuario=findViewById(R.id.cajaUsuario);
        CajaCorreo=findViewById(R.id.cajaCorreo);
        CajaMatricula=findViewById(R.id.cajaMatricula);
        CajaContrasena=findViewById(R.id.cajaContrasena);

        BtnVaciar=findViewById(R.id.btnVaciar);
        BtnVaciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CajaUsuario.setText("");
                CajaUsuario.setHint("Nombre Usuario");
                CajaCorreo.setText("");
                CajaCorreo.setHint("Correo (UANL)");
                CajaMatricula.setText("");
                CajaMatricula.setHint("Matricula");
                CajaContrasena.setText("");
                CajaContrasena.setHint("Contraseña");
            }
        });

        BtnRegistrar=findViewById(R.id.btnRegistro);
        BtnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper insert = new DBHelper(getApplicationContext());
                SQLiteDatabase ise = insert.getWritableDatabase();
                ContentValues datos = new ContentValues();
                datos.put("NOMBREUSUARIO", CajaUsuario.getText().toString());
                datos.put("CORREO", CajaCorreo.getText().toString());
                datos.put("MATRICULA", CajaMatricula.getText().toString());
                datos.put("CONTRASENA", CajaContrasena.getText().toString());
                ise.insert("USUARIOS", null, datos);
                ise.close();

                finish();
                Intent registrar=new Intent(Registro.this, Login.class);
                startActivity(registrar);
                Toast.makeText(Registro.this, "Bienvenido "+CajaUsuario.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
