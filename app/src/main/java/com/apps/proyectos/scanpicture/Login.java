package com.apps.proyectos.scanpicture;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    Button BtnRegistro, BtnIniciar;
    EditText CajaUsuario, CajaContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        CajaUsuario = findViewById(R.id.cajaUsuario);
        CajaContrasena = findViewById(R.id.cajaContrasena);

        BtnRegistro = findViewById(R.id.btnRegistro);
        BtnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent acceso = new Intent(Login.this, Registro.class);
                startActivity(acceso);
            }
        });

        BtnIniciar = findViewById(R.id.btnIniciar);
        BtnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CajaUsuario.getText().toString().equals("") && CajaContrasena.getText().toString().equals("")){
                    Toast.makeText(Login.this, "Ingrese o registre un usuario y contraseña", Toast.LENGTH_SHORT).show();
                }else{
                    DBHelper consulta = new DBHelper(getApplicationContext());
                    SQLiteDatabase bd = consulta.getWritableDatabase();
                    @SuppressLint("Recycle") Cursor fila = bd.rawQuery("SELECT * FROM USUARIOS", null);
                    if (fila.moveToNext()) {
                        finish();
                        Intent acceso = new Intent(Login.this, MenuInicio.class);
                        startActivity(acceso);
                        Toast.makeText(Login.this, "Bienvenido a LectorScan!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
