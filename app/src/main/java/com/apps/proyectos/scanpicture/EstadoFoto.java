package com.apps.proyectos.scanpicture;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EstadoFoto extends AppCompatActivity {

    ListView ListaPeriodo;
    TextView TxtDatoNumero;

    ArrayList<String> profesores=new ArrayList<>();
    ArrayList<String> materias=new ArrayList<>();
    ArrayList<String> horas=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_foto);

        TxtDatoNumero=findViewById(R.id.txtDatoNumero);
        Bundle data=getIntent().getExtras();
        assert data != null;
        TxtDatoNumero.setText(data.getString("dato"));

        llenarDatos();

        ListaPeriodo=findViewById(R.id.listaPeriodo);
        ListaPeriodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                View v = new View(EstadoFoto.this);
                final AlertDialog verClase = new AlertDialog.Builder(EstadoFoto.this)
                        .setView(v)
                        .setPositiveButton("Aceptar", null)
                        .create();

                verClase.setTitle("DATOS");

                verClase.setCancelable(false);

                //Agregas los campos
                final TextView nombre = new TextView(EstadoFoto.this);
                final TextView materia = new TextView(EstadoFoto.this);
                final TextView hora = new TextView(EstadoFoto.this);

                //Ajuste de vistas
                nombre.setGravity(Gravity.CENTER_HORIZONTAL);
                materia.setGravity(Gravity.CENTER_HORIZONTAL);
                hora.setGravity(Gravity.CENTER_HORIZONTAL);

                nombre.setText("Nombre: "+profesores.get(position));
                materia.setText("Materia: "+materias.get(position));
                hora.setText("Hora Clase: "+horas.get(position));

                //Creacion del Layout o acomodo de los objetos
                LinearLayout acomodar = new LinearLayout(EstadoFoto.this);
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(nombre);
                acomodar.addView(materia);
                acomodar.addView(hora);
                verClase.setView(acomodar);

                verClase.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button positivo=(verClase).getButton(AlertDialog.BUTTON_POSITIVE);
                        positivo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                verClase.dismiss();
                            }
                        });
                    }
                });

                verClase.show();
            }
        });

        Button BtnRegreso=findViewById(R.id.btnRegreso);
        BtnRegreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent regresar=new Intent(EstadoFoto.this, MenuInicio.class);
                startActivity(regresar);
            }
        });
    }

    private void llenarDatos() {
        String URL = "http://classcam.herokuapp.com/classroms.json?search="+TxtDatoNumero.getText().toString();

        Log.i("url", "" + URL);

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jinicial = new JSONArray(response);
                    JSONObject job=new JSONObject(jinicial.get(0).toString());
                    JSONArray jperiodos=job.getJSONArray("periods");
                    for(int i=0;i<jperiodos.length();i++){
                        JSONObject objeto=new JSONObject(jperiodos.get(i).toString());
                        String profesor=objeto.getString("teacher");
                        profesores.add(profesor);
                        String materia=objeto.getString("assignment");
                        materias.add(materia);
                        JSONObject job3hora=new JSONObject(objeto.get("hour").toString());
                        String hora= job3hora.getString("name");
                        horas.add(hora);
                        //Toast.makeText(EstadoFoto.this, " "+profesor+" ,"+materia+" ,"+hora, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error en la carga del contenido", Toast.LENGTH_LONG).show();
                }
                ArrayAdapter<String> adaptador=new ArrayAdapter<>(EstadoFoto.this, android.R.layout.simple_list_item_1,profesores);
                ListaPeriodo.setAdapter(adaptador);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }
}
