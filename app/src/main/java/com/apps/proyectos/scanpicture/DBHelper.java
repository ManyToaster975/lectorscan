package com.apps.proyectos.scanpicture;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    DBHelper(Context context) {
        super(context, "lectorscanbd", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE USUARIOS(CLAVEUSUARIO INTEGER PRIMARY KEY AUTOINCREMENT, NOMBREUSUARIO TEXT, CORREO TEXT, MATRICULA TEXT, CONTRASENA TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS USUARIOS");

        onCreate(sqLiteDatabase);
    }
}